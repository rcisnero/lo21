#if !defined(_EVENEMENT_H)
#define _EVENEMENT_H

#include <iostream>
#include <string>
#include <vector>
#include "timing.h"

namespace TIME {

	class Evt {
	private:
		std::string sujet;
	public:
		Evt(const std::string& s) : sujet(s) {
			std::cout << "construction d'un objet Evt" << std::endl;
		}
		// Methode virtuelle pure -> egal � 0 au moment de la d�finition dans la classe abstraite
		// Les classes filles devront redefinir cette fonction
		virtual void afficher(std::ostream& f = std::cout) const = 0;
		const std::string& getDescription() const { return sujet; }
		virtual ~Evt() { std::cout << "destruction d'un objet Evt" << std::endl; }
	};

	class EvtPj : public Evt {
	private:
		Date debut;
		Date fin;
	public:
		EvtPj(const Date& d1, const Date& d2, const std::string& s) : Evt(s), debut(d1), fin(d2) {
			std::cout << "construction d'un objet EvtPj" << std::endl;
		}
		const Date& getDateDebut() const { return debut; }
		const Date& getDateFin() const { return fin; }
		// Ce n'est pas nec�ssaire de la methode virtuelle car on herite de la classe abstraite Evt
		/*virtual*/ void afficher(std::ostream& f = std::cout) const;
	};

	class Evt1j : public Evt {
	private:
		Date date;
	public:
		Evt1j(const Date& d, const std::string& s) : Evt(s), date(d) {
			std::cout << "construction d'un objet Evt1j" << std::endl;
		}
		
		const Date& getDate() const { return date; }

		// Lien dynamique avec les classes filles avec virtaul
		// Exo 34 : Ce n'est pas nec�ssaire de la methode virtuelle car on herite de la classe abstraite Evt
		/*virtual*/ void afficher(std::ostream& f = std::cout) const;
		/*virtual*/ ~Evt1j() { std::cout << "destruction d'un objet Evt1j" << std::endl; }
	};

	class Evt1jDur : public Evt1j {
	private :
		Horaire debut;
		Duree duree;
	public:
		Evt1jDur(const Date& d, const std::string& s, const Horaire& h, const Duree& t):
			Evt1j(d,s),//appel au constructeur de la classe de base
			debut(h),duree(t) {
			std::cout << "construction d'un objet Evt1jDur" << std::endl;
		}
		~Evt1jDur() { std::cout << "destruction d'un objet Evt1jDur" << std::endl; }
		const Horaire& getHoraire() const { return debut; }
		const Duree& getDuree() const { return duree; }
		void afficher(std::ostream& f = std::cout) const;
	};

	class Rdv : public Evt1jDur {
	private:
		std::string	personne;
		std::string lieu;
	public:
		Rdv(const Date& d, const std::string& s, const Horaire& h, const Duree& t,
			const std::string& p, const std::string& l) :
			Evt1jDur(d, s, h, t), personne(p), lieu(l) {
			std::cout << "construction d'un objet Rdv" << std::endl;
		}
		~Rdv() { std::cout << "destruction d'un objet Rdv" << std::endl; }
		const std::string& getPersonne() const { return personne; }
		const std::string& getLieu() const { return lieu; }
		void afficher(std::ostream& f = std::cout) const;
	};

	class Agenda {
	private:
		// Avec le tableau de pointeurs, on peut pointer vers un objet Evt1jDur ou Rdv
		std::vector<Evt*> tab;
		int nbEvts = 0;
	public:
		// On appelle le constructeur par d�faut d'Agenda -> de tab
		Agenda() = default; // Agenda() : tab() {}
		Agenda(const Agenda&) = delete;
		Agenda& operator=(const Agenda&) = delete;
		Agenda& operator<<(Evt& e);
		void afficher(std::ostream& f = std::cout) const;

		// Methodes nec�ssaires :
		// begin, end, ++, -- (car bidirectionnel)
		class iterator : public std::vector<Evt*>::iterator {
		private:
			friend class Agenda;
			iterator(const std::vector<Evt*>::iterator& it) : 
				std::vector<Evt*>::iterator(it) {}
		public:
			// �a affiche la valeur point�e par le vecteur
			Evt& operator*() {
				return *std::vector<Evt*>::iterator::operator*();
			}

		};
		iterator begin() { return iterator(tab.begin()); }
		iterator end() { return iterator(tab.end()); }

		class const_iterator : public std::vector<Evt*>::const_iterator {
		private:
			friend class Agenda;
			const_iterator(const std::vector<Evt*>::const_iterator& it) :
				std::vector<Evt*>::const_iterator(it) {}
		public:
			// �a affiche la valeur point�e par le vecteur
			Evt& operator*() {
				return *std::vector<Evt*>::const_iterator::operator*();
			}

		};
		const_iterator cbegin() const { return const_iterator(tab.begin()); }
		const_iterator cend() const { return const_iterator(tab.end()); }
	};
}

// On evite de surcharger un operator<< dans un namespace
std::ostream& operator<<(std::ostream& f, const TIME::Evt1j& e);

#endif
