#include "evenement.h"

std::ostream& operator<<(std::ostream& f, const TIME::Evt1j& e) {
	e.afficher(f);
	return f;
}

// Deplacement des methodes afficher car elles osnt virtuelles -> pas compatible avec fonction inline
// Il n'y a pas besoin de definir la methode comme virtual, c'est d�j� declar�e dans evenement.h
void TIME::Evt1j::afficher(std::ostream& f ) const {
	f << "***** Evt ********" << "\n" << "Date=" << date 
		<< " sujet=" << getDescription() << "\n";
}

// Exercice 35, classe EvtPj
void TIME::EvtPj::afficher(std::ostream& f) const {
	f << "***** Evt ********" << "\n" << "Date debut=" << debut << " Date fin=" << fin
		<< " sujet=" << getDescription() << "\n";
}

void TIME::Evt1jDur::afficher(std::ostream& f ) const {
	//f << "***** Evt ********" << "\n" << "Date=" << getDate() << " sujet=" << getDescription() << "\n";
	Evt1j::afficher(f); // rappel de la m�thode de la classe de base
	f << "debut=" << debut << " duree=" << duree << std::endl;
}

void TIME::Rdv::afficher(std::ostream& f ) const {
	Evt1jDur::afficher(f); // rappel de la m�thode de la classe de base
	f << "personne=" << personne << " lieu=" << lieu << std::endl;
}

// Exercice 34
TIME::Agenda& TIME::Agenda::operator<<(TIME::Evt& e) {
	tab.push_back(&e);
	nbEvts++;
	// renvoyer ce qui est � gauche de <<
	return *this;
}

void TIME::Agenda::afficher(std::ostream& f) const {
	if (tab.empty()) throw "Il n'y a pas d'événements dans agenda";

	f << "### AGENDA ###" << std::endl;
	for (int i = 0; i < nbEvts; i++){
		// tab va appeller la methode afficher selon le type d'objet qu'il contient 
		tab[i]->afficher(f);
	}
	f << "### FIN D'AGENDA ###" << std::endl;
}