#include <iostream>
#include "evenement.h"

int main() {
	using namespace std;
	using namespace TIME;

	// Ligne 21 de evenement.h, methode afficher virtuelle -> on n'affiche pas toute l'info
	
	/*
	Agenda a;
	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Rdv e2(Date(11, 4, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");
	EvtPj e3(Date(1, 10, 1957), Date(10, 10, 1957), "Oktoberfest");

	a << e1 << e2 << e3;
	
	a.afficher();
	*/

	
	//-- EXERCICE 33 - Q2 --
	Rdv* pt5 = new Rdv(Date(12, 11, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV","bureau");
	pt5->afficher();
	delete pt5;

	Evt1j * pt6 = new Rdv(Date(12, 11, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV","bureau");
	pt6->afficher();
	delete pt6;
	

	/*
	-- EXERCICE 33 - Q1 --
	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	Evt1jDur e3(Date(11, 6, 2013), "Lancement de Longue Marche", Horaire(17, 38), Duree(0, 10));
	Rdv e4(Date(11, 4, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");
	e1.afficher(); e2.afficher(); e3.afficher(); e4.afficher();
	Evt1j * pt1 = &e1; Evt1j * pt2 = &e2; Evt1j * pt3 = &e3; Evt1j * pt4 = &e4;
	//pt1->afficher(); pt2->afficher(); pt3->afficher(); pt4->afficher();
	cout << e1 << e2 << e3 << e4;
	*/

	/*
	-- CODE POUR L'EXERCICE 31 --
	Evt1j* tab[2];
	Evt1j evt(Date(1 , 12 , 2021), "TD");
	Rdv rdv(Date(2, 12, 2021), "examen", Horaire(8, 0), Duree(90), "Intervenants UV",
		"HDS");
	tab[0] = &evt;
	tab[1] = &rdv;

	for (size_t i = 0; i < sizeof(tab) / sizeof(tab[0]); i++)
		tab[i]->afficher();

	*/

	return 0;
}