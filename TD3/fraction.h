#pragma once
#include <iostream>
#include <string.h>

class FractionException {
	char info[256];
public:
	FractionException(const char* str) { strcpy(info, str); }
	const char* getInfo() const { return info; }
};

namespace MATH {
	class Fraction {
	private:
		// Avec les constructeurs par initialisation, on peut definir les attributs
		// (numerateur = 0; denominateur = 1;)
		int numerateur;
		int denominateur;
		void simplification();

	public:
		/*
		----------------------------------
		// Constructeurs par affectation
		Fraction(int n, int d) { setFraction(n, d); }
		Fraction(int n) { setFraction(n, 1); }
		Fraction() { setFraction(0, 1); } // Si on ne definit pas le constructeur sans arguments, on est oblig�s de faire un objet avec arguments
		----------------------------------
		----------------------------------
		// Constructeurs par initialisation
		Fraction(int n, int d) : numerateur(n), denominateur(d) {
			if (denominateur == 0) {
				std::cerr << "Erreur : denominateur = 0" << std::endl;
				denominateur = 1;
			}
		}
		Fraction(int n) : numerateur(n) {} // , denominateur(1) -- grace � l'initialisation des attributs (denominateur = 1;)
		Fraction() {} // : numerateur(0), denominateur(1) -- grace � l'initialisation des attributs (numerateur = 0; denominateur = 1;)
		----------------------------------
		*/

		// Un seul constructeur
		Fraction(int n = 0, int d = 1) : numerateur(n), denominateur(d) {
			if (d == 0) {
				throw FractionException("erreur : denominateur = 0");
			}
			std::cout << "Construction de la fraction " << this << std::endl;
			simplification();
		}

		// Accesseurs en lecture et edition
		int getNumerateur() const { return numerateur; } // La methode est const, La methode ne modifie pas les attributs de la classe
		int getDenominateur() const { return denominateur; }
		void setFraction(int n, int d);

		// Definition de somme comme m�thode de la classe Fraction
		// Type de retour : valeur
		// 1ere const : La valeur de retour est const;
		// 2eme const : La methode est const. La fraction de l'objet qui appele la methode NE pourra PAS etre modifi� : 
		const Fraction somme(const Fraction& f) const;
		
		// Destructeur
		~Fraction() {
			std::cout << "Destruction de la fraction " << this << std::endl;
		}

		// Surcharge de l'op�rateur +
		// Definition de somme comme m�thode de la classe Fraction
		const Fraction operator+(const Fraction& f) const;

		// Surcharge de l'operateur ++
		// PREFIXE
		// Renvoie une reference de la valeur  increment�e
		Fraction& operator++();

		//POSTFIXE
		// Renvoie une valeur avant que la valeur soit increment�e
		const Fraction operator++(int);
	};

	// Definition de somme comme fonction externe
	// Type de retour : valeur
	// Il peut co-habiter avec la definition comme m�thode
	const Fraction somme(const Fraction& f1, const Fraction& f2);

	// Surcharge de l'op�rateur +
	// Definition de somme comme fonction externe
	// Il NE peut PAS co-habiter avec la definition comme m�thode
	//const Fraction operator+(const Fraction& f1, const Fraction& f2);

	// Definition de la surcharge operator + avec un int en premier lieu
	const Fraction operator+(const int i, const Fraction& f);

}

// Function non membre car le premier argument est une r�f�rence sur un objet std::ostream
// On ne le met pas dans le namespace MATH car on aurait besoin d ecrire MATH::cout � chaque fois
// Renvoie une r�f�rence afin d'avoir la possibilit� d'une insertion multiple (cout << t1 << t2 << ...)
std::ostream& operator<<(std::ostream& f, const MATH::Fraction& frac);
