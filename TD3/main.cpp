#include "fraction.h"

using namespace std;
using namespace MATH;

int main() {
	/*
	Fraction f1(1, 2);
	Fraction f2(3, 4);
	Fraction f3;
	//f3 = somme(f1, f2);
	//f3 = f1.somme(f2);

	// Type de retour const : On ne veut pas qu'une somme de fractions soit affectable
	// somme(f1, f2) = Fraction(0) -- ERREUR
	f3 = f1 + f2;

	//f3 = f1 + 6; // Utilise la methode de la classe : f1.operateur+(6)
	//f3 = 6 + f1; // ERREUR : on n'a pas de fonction qui prend en premier lieu un int
	// Corrig� avec la surcharge de l'operator+ en externe (ligne 69)
	
	cout << f3;

	f3++;
	cout << f3;
	*/
	
	//Fraction B(0, 0);
	try {
		Fraction A(1, 2);
		Fraction B(0, 0);
	}
	catch (FractionException f) {
		cout << f.getInfo() << endl;
	}
	
	return 0;
}

/* EXERCICE 20 - Q6
Fraction* myFunction() { 
	Fraction fx(7, 8);
	Fraction* pfy = new Fraction(2, 3); 
	return pfy; 

	// fx est detruit � la fin de la fonction
	// pfy est maintenu grace � l'allocation dynamique
}

int main() { 
	Fraction f1(3, 4); 
	Fraction f2(1, 6); 
	// Ces deux fraction ci-dessus sont d�truits apr�s la fin du main

	Fraction* pf3 = new Fraction(1, 2); 
	cout << "ouverture d�un bloc\n"; 
	Fraction* pf6; 
	
	{
		Fraction f4(3, 8); 
		Fraction f5(4, 6); 
		// Ces deux fraction ci-dessus sont d�truits apr�s la fin du bloc

		pf6 = new Fraction(1, 3); 
		// Par contre, le pointeur est allou� dynamiquement est ce n'est pas d�truit
	}
	cout << "fin d�un bloc\n"; 
	cout << "debut d�une fonction\n"; 
	Fraction* pf7 = myFunction(); 
	cout << "fin d�une fonction\n";
	cout << "desallocations controlee par l�utilisateur :\n"; 
	delete pf6;
	delete pf7; 
	
	return 0; 
}
*/




