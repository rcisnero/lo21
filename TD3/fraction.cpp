#include "fraction.h"

// 1. Namespace -> 2. Classe -> 3. Methode
void MATH::Fraction::setFraction(int n, int d) { 
	if (d == 0) {
		throw FractionException("erreur : denominateur = 0");
	}
	numerateur = n;
	denominateur = d;
	simplification();
}

void MATH::Fraction::simplification() {
	// si le numerateur est 0, le denominateur prend la valeur 1
	if (numerateur == 0) { denominateur = 1; return; }

	/*un denominateur ne devrait pas �tre 0;
	si c�est le cas, on sort de la m�thode*/
	if (denominateur == 0) return;

	/*utilisation de l�algorithme d�Euclide pour trouver le Plus Grand Commun
	Denominateur (PGCD) entre le numerateur et le denominateur*/
	int a = numerateur, b = denominateur;

	// on ne travaille qu�avec des valeurs positives...
	if(a < 0) a = -a; 
	if(b < 0) b = -b;

	while(a != b){ 
		if (a > b) a = a-b; 
		else b = b-a; 
	}

	// on divise le numerateur et le denominateur par le PGCD = a
	numerateur /= a; denominateur /= a;

	// si le denominateur est n�gatif, on fait passer le signe - au denominateur
	if (denominateur < 0) { 
		denominateur -= denominateur; 
		numerateur -= numerateur; 
	}
}

// Definition de la methode somme
const MATH::Fraction MATH::Fraction::somme(const Fraction& f) const {
	return Fraction(numerateur * f.denominateur + f.numerateur * denominateur, 
		denominateur * f.denominateur);
}

// Definition de la focntion externe somme
// On a besoin des accesseurs en lecture car on n'a pas acc�s aux attributs
const MATH::Fraction MATH::somme(const Fraction& f1, const Fraction& f2) {
	return Fraction(f1.getNumerateur() * f2.getDenominateur() + f1.getNumerateur() * f2.getDenominateur(),
		f1.getDenominateur() * f2.getDenominateur());
}

// Definition de la surcharge + (methode)
const MATH::Fraction MATH::Fraction::operator+(const Fraction& f) const {
	return somme(f);
}

/*
Definition de la surcharge + (fonction externe)
const MATH::Fraction MATH::operator+(const Fraction& f1, const Fraction& f2) {
}
*/

// Definition de la surcharge avec un int en premier lieu
const MATH::Fraction MATH::operator+(const int i, const Fraction& f) {
	return Fraction(f.getNumerateur() * 1 + f.getNumerateur() * 1,
		f.getDenominateur() * 1);
}

// Surcharge de l'operateur ++
// Methodes membres : on peut acceder directement aux attributs (ou aux accesseurs)
// PREFIXE
MATH::Fraction& MATH::Fraction::operator++() {
	setFraction(numerateur + getDenominateur(), getDenominateur());
	return *this;
}

//POSTFIXE
const MATH::Fraction MATH::Fraction::operator++(int) {
	Fraction f(numerateur, denominateur); // copie de la fraction
	setFraction(numerateur + getDenominateur(), getDenominateur());
	return f;
}

// Surcharge de operator<<
std::ostream& operator<<(std::ostream& f, const MATH::Fraction& frac) {
	f << frac.getNumerateur();
	if (frac.getDenominateur() != 1)
		f << "/" << frac.getDenominateur();
	return f;
}
