# LO21

Programmation et conception orientées objet

[![C++](https://img.shields.io/badge/-C++-success.svg)](https://gitlab.utc.fr/rcisnero/lo21/)
[![UML](https://img.shields.io/badge/-UML-important.svg)](https://gitlab.utc.fr/rcisnero/lo21/)
[![DesignPatterns](https://img.shields.io/badge/-DesignPatterns-informational.svg)](https://gitlab.utc.fr/rcisnero/lo21/)
