#pragma once
#include<map>
#include<set>
#include<string>
#include<iostream>

using namespace std;

// Tous les patrons doivent etre definis dans le .h
template<class Vertex>
class GraphG {
	/** Un objet map est une liste de cl�s et de valeurs associ�es � cette cl�
	* 
	*/
	map<Vertex, set<Vertex> > adj;
	string name;
	unsigned int nb_edges;
public:
	GraphG(const string& n) : name(n) {}
	const string& getName() const { return name; }
	size_t getNbVertices() const { return adj.size(); } //nb Sommets, chaque sommets est une entr�e dans le map
	size_t getNbEdges() const; //nb Arcs
	
	void addVertex(const Vertex& i) { 
		// Reviser la documentation de la methode operator[] de map
		// S'il existe d�j�, on renvoie la liste de valeurs associ�es
		// Sinon; on cr�e l'element cl� et une liste vide de valeurs associ�es
		adj[i];  

		// Methode eqiuvalent
		// adj.insert(make_pair(i, set<Vertex>());
	}

	void addEdge(const Vertex& i, const Vertex& j);
	void removeEdge(const Vertex& i, const Vertex& j);
	void removeVertex(const Vertex& i);
	void print(ostream& f) const;
};

template<class V> ostream& operator<<(ostream& f, const GraphG<V>& G);

template<class V>
size_t GraphG<V>::getNbEdges() const{
	size_t nb = 0;
	for (auto it = adj.begin(); it != adj.end(); ++it) {
		nb += it->second.size(); // taille de l'ensemble de sommets
		// plus d'info : struct Pair de la STL
		// first : element qui pointe vers la liste de cl�s
		// second : element qui pointe vers la liste de valerus associ�es aux cl�s

		// Fa�on eauivalente
		//for (auto& p : adj) nb += p.second.size();
	}
	return nb;
}

template<class Vertex>
void GraphG<Vertex>::addEdge(const Vertex& i, const Vertex& j) {
	// Si l'arc existe d�j�, il y a rien qui se passe
	// Il n'y a pas de doublons dans le set<Vertex>
	adj[i].insert(j);
	
	// On ajoute l'autre sommet (operator[] g�re la verification de son existance)
	adj[j];
}

template<class Vertex>
void GraphG<Vertex>::removeEdge(const Vertex& i, const Vertex& j) {
	auto iti = adj.find(i);
	if (iti != adj.end()) { // le sommet i existe
		//la methode erase retourne 1 si la valeur est correctement supprim�e
		if (iti.second.erase(j) != 1)
			throw "erreur, l'arc n'existe pas";
	}
	else throw "erreur, le sommet n'existe pas";
}

template<class Vertex>
void GraphG<Vertex>::removeVertex(const Vertex& i) {
	//On doit supprimer le sommet lui m�me, 
	//mais aussi des listes de valeurs associ�es des autres sommets
	
	auto it = adj.find(i);
	if (it != adj.end()) {
		// 1. On supprime l'entr�e i de la map
		adj.erase(it);

		// 2. On supprime i des liste de successeurs
		for (auto& p : adj) p.second.erase(i);
	}
	else throw "erreur, le sommet n'existe pas";
}

template<class Vertex>
void GraphG<Vertex>::print(ostream& f) const {
	f << "Graphe " << name << "\n";
	for (auto& p : adj) {
		f << p.first << " : ";
		for (auto& v : p.second) f << v << " ; ";
		f << "\n";
	}

}

