#include "graph.h"

int main() {
	GraphG<char> G1("G1");
	G1.addVertex('a');
	G1.addVertex('b');
	G1.addVertex('c');

	G1.addEdge('a', 'b');
	G1.addEdge('a', 'd');
	
	G1.print(std::cout);
	return 0;
}