#if !defined(_Stack_T_H)
#define _Stack_T_H

#include "vector.h"

namespace AC { // adapteur de classe
	template<class T, class CONT = TD::Vector<T>>
	class Stack : private CONT {
	public:
		Stack() : CONT(0) {}
		void push(const T& x) { CONT::push_back(x); }
		void pop() { CONT::pop_back(); }
		const T& top() const { return CONT::back(); }
		size_t size() const { return CONT::size(); }
		T& top() { return CONT::back(); }
		bool empty() const { return CONT::empty(); } // /*ou alors :*/ using CONT::empty;
		void clear() { CONT::clear(); } // /*ou alors :*/ using CONT::clear;

		// Deuxieme utilisation du mot cl� typename
		using iterator = typename CONT::iterator;
		using const_iterator = typename CONT::const_iterator;
		iterator begin() { return CONT::begin(); }
		iterator end() { return CONT::end(); }
		const_iterator begin() const { return CONT::begin(); }
		const_iterator end() const { return CONT::end(); }
	};
}

namespace AO { // adapteur d�objet
	template<class T, class CONT = TD::Vector<T>>
	class Stack {
		CONT cont;
	public:
		Stack() :cont(0) {}
		void push(const T& x) { cont.push_back(x); }
		void pop() { cont.pop_back(); }
		const T& top() const { return cont.back(); }
		T& top() { return cont.back(); }
		size_t size() const { return cont.size(); }
		bool empty() const { return cont.empty(); }
		void clear() { cont.clear(); }

		// Deuxieme utilisation du mot cl� typename
		using iterator = typename CONT::iterator;
		using const_iterator = typename CONT::const_iterator;
		iterator begin() { return cont.begin(); }
		iterator end() { return cont.end(); }
		const_iterator begin() const { return cont.begin(); }
		const_iterator end() const { return cont.end(); }

	};
}

template<class IT>
IT element_minimum(IT it1, IT it2) {
	IT itmin = it1;
	while (it1 != it2) {
		if (*it1 < *itmin) itmin = it1;
		++it1;
	}
	return itmin;
}

// FUNC est le type de la fonction de comparaison
template<class IT, class FUNC>
IT element_minimum(IT it1, IT it2, FUNC comp) {
	IT itmin = it1;
	// comp est la fonction de comparaison ou l'objet fonction ou une lambda
	while (it1 != it2) {
		if (comp(*it1, *itmin)) itmin = it1;
		++it1;
	}
	return itmin;
}

#endif