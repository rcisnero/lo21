#include "container.h"
#include "stack.h"
#include "vector.h"
#include <list>
#include <iostream>

bool sup(int a, int b) { return a > b; }
// ou 
class Sup {
public:
	bool operator()(int a, int b) const {
		return a > b;
	}
};

int main() {
	using namespace TD;
	Vector<int> t1(10, 5);
	Vector<int> t2(10);
	t1.push_back(2);
	t1.push_back(7);
	t1.push_back(12);
	t1.push_back(11);
	t1.push_back(10);
	t1.pop_back();

	auto it = element_minimum(t1.begin(), t1.end());
	std::cout << "min=" << *it << std::endl;

	it = element_minimum(t1.begin(), t2.end(), sup);
	std::cout << "max=" << *it << std::endl;

	Sup scomp;
	it = element_minimum(t1.begin(), t2.end(), scomp);
	// ou
	//it = element_minimum(t1.begin(), t2.end(), Sup());
	std::cout << "max2=" << *it << std::endl;

	return 0;
}