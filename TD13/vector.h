#if !defined(_Vector_T_H)
#define _Vector_T_H

namespace TD {
	template<class T> class Vector : public Container<T> {
		T* tab;
		size_t cap;
	public:
		Vector(size_t s = 0, const T& x = T());
		Vector(const Vector<T>& v);
		~Vector();
		Vector<T>& operator=(const Vector<T>& v);
		T& operator[](size_t i) { return tab[i]; }
		const T& operator[](size_t i) const { return tab[i]; }
		T& element(size_t i) override;
		const T& element(size_t i) const override;
		void push_back(const T& x) override;
		void pop_back() override;
		
		using iterator = T*;
		using const_iterator = const T*;
		iterator begin() { return tab; }
		iterator end() { return tab + this->cap; }
		const_iterator begin() const { return tab; }
		const_iterator end() const { return tab + this->cap; }
	};

	/* 
	d�j� dans le fichier stack.h

	namespace AO {
		template<class T, class CONT = Vector<T>>
		class Stack : private Vector<T> {
			CONT cont;
			bool empty() const { return cont.empty(); }
			void push(const T& x) { return cont.push_back(); }
			void pop() { return cont.pop_back(); }
			size_t size() const { return cont.size(); }
			T& top() { return cont.back(); }
			const T& top() const { return cont.back(); }
			void clear() { return cont.clear(); }

		};
	}

	namespace AC {
		template<class T, class CONT = Vector<T>>
		class Stack : private CONT {
			bool empty() const { return CONT<T>::empty(); }
			void push(const T& x) { return this->push_back(); }
			void pop() { return this->pop_back(); }
			size_t size() const { return CONT<T>::size(); }
			T& top() { return this->back(); }
			const T& top() const { return this->back(); }
			void clear() { return CONT<T>::clear(); }

		};
	}
	*/
}

template<class T> TD::Vector<T>::Vector(size_t s, const T& x) :Container<T>(s), tab(new T[s]), cap(s) {
	for (size_t i = 0; i < Container<T>::nbEl; i++)
		tab[i] = x;
}

template<class T> TD::Vector<T>::Vector(const Vector<T>& v) : Container<T>(v.nbEl), tab(new T[v.size()]), cap(v.size()) {
	for (size_t i = 0; i < v.size(); i++)
		tab[i] = v.tab[i];
}

template<class T> TD::Vector<T>::~Vector() {
	delete[] tab;
}

template<class T> TD::Vector<T>& TD::Vector<T>::operator=(const Vector<T>& v) {
	if (this != &v) {
		T* newtab = new T[v.nbEl];
		for (size_t i = 0; i < v.nbEl; i++)
			newtab[i] = v.tab[i];
		cap = v.nbEl;
		Container<T>::nbEl = v.nbEl;
		T* old = tab;
		delete[] old;
		tab = newtab;
	}
	return *this;
}

template<class T> T& TD::Vector<T>::element(size_t i) {
	if (i < Container<T>::nbEl) return tab[i];
	throw ContainerException("erreur Container : demande hors limite (conteneur avec moins de i �l�ments)");
}

template<class T> const T& TD::Vector<T>::element(size_t i) const {
	if (i < Container<T>::nbEl) return tab[i];
	throw ContainerException("erreur Container : demande hors limite (conteneur avec moins de i �l�ments)");
}

template<class T> void TD::Vector<T>::push_back(const T& x) {
	if (Container<T>::nbEl == cap) {
		T* newtab = new T[cap + 5];
		for (size_t i = 0; i < Container<T>::nbEl; i++)
			newtab[i] = tab[i];
		T* old = tab;
		cap += 5;
		tab = newtab;
		delete[] old;
	}
	tab[Container<T>::nbEl++] = x;
}

template<class T> void TD::Vector<T>::pop_back() {
	if (!this->empty())
		Container<T>::nbEl--;
	else
		throw ContainerException("erreur Container : demande hors limite (container vide)");
}

#endif