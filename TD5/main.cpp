#include "set.h"
using namespace Set;

int main() {
	try {
		Controleur c;
		c.distribuer();
		cout << c.getPlateau();
		c.distribuer();
		cout << c.getPlateau();
	}
	catch (SetException& e) {
		std::cout << e.getInfo() << "\n";
	}
	
	return 0;
}