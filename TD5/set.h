#ifndef _SET_H
#define _SET_H

#include <iostream>
#include <string>
#include <initializer_list>
#include <array>
#include <cstdlib>
using namespace std;

namespace Set {
	// classe pour gérer les exceptions dans le set
	class SetException {
	public:
		SetException(const string& i) :info(i) {}
		string getInfo() const { return info; }
	private:
		string info;
	};

	// caractéristiques
	enum class Couleur { rouge, mauve, vert };
	enum class Nombre { un = 1, deux = 2, trois = 3 };
	enum class Forme { ovale, vague, losange };
	enum class Remplissage { plein, vide, hachure };

	// conversion d'une caractéristique en string
	string toString(Couleur c);
	string toString(Nombre v);
	string toString(Forme f);
	string toString(Remplissage v);

	// écriture d'une caractéristique sur un flux ostream
	ostream& operator<<(ostream& f, Couleur c);
	ostream& operator<<(ostream& f, Nombre v);
	ostream& operator<<(ostream& f, Forme x);
	ostream& operator<<(ostream& f, Remplissage r);

	// listes contenant les valeurs possibles pour chacune des caractéristiques
	extern std::initializer_list<Couleur> Couleurs;
	extern std::initializer_list<Nombre> Nombres;
	extern std::initializer_list<Forme> Formes;
	extern std::initializer_list<Remplissage> Remplissages;

	// affichage des valeurs possibles pour chaque caractéristiques
	void printCouleurs(std::ostream& f = cout);
	void printNombres(std::ostream& f = cout);
	void printFormes(std::ostream& f = cout);
	void printRemplissages(std::ostream& f = cout);

	class Carte {
		Couleur couleur;
		Nombre nombre;
		Forme forme;
		Remplissage remplissage;
		Carte(Couleur c, Nombre n, Forme f, Remplissage r) :couleur(c), nombre(n), forme(f), remplissage(r) {};
		// Q3
		friend class Jeu;
	public:
		
		Couleur getCouleur() const { return couleur; }
		Nombre getNombre() const { return nombre; }
		Forme getForme() const { return forme; }
		Remplissage getRemplissage() const { return remplissage; }
		~Carte() = default; //optionnel
		Carte(const Carte& c) = default; //optionnel
		Carte& operator=(const Carte& c) = default; //optionnel
	};

	/* Possibilités de singleton :
	*	1. un methode static qui cré les objets Jeu static. 
			Obtenir une instace qui n'est pas geré par l'utilisateur. On retourne le méme objet
		2. un pointeur sur un objet Jeu. 
			Pointeur initialisé avec nullptr et on vérifie é chaque appel de la foncion si le pointeur est "alimenté"
		3. Une strcuture encapsulée "Handler". 
			Cela permet de appeler le destructeur de Handler à la fin de l'ex�cution du programme.
	*/
	class Jeu {
		const Carte* cartes[81];
		// On a plac� les 4 methodes en prive. Il n'est pas possible de cr�er objets en explicit
		Jeu();
		~Jeu();
		Jeu(const Jeu&) = delete;
		Jeu& operator=(const Jeu&) = delete;

		// Pointeur sur l'objet instance pour gerer le singleton
		// Pour initialiser un attribut static, c'est dans le cpp
		static Jeu* instance;

		// 3eme fa�on. 
		/*
		struct Handler {
			Jeu* instance;
			Handler():instance(nullptr){}
			~Handler() { delete instance; }
		};
		static Handler handler;
		*/

		// Avec l'implementation de iterator, on deplace la methode en priv�
		const Carte& getCarte(size_t i) const {
			if (i >= 81)
				throw SetException("Carte invalide");
			return *cartes[i];
		}

		// Iterator peut acc�der � getCarte, methode priv� de Jeu
		friend class Iterator;
		friend class FormeIterator;

	public:
		class Iterator {
			size_t i = 0;
			Iterator() = default;
			// Cela permet � un objet Jeu d'acceder au constructeur priv� de iterateur
			friend class Jeu;
		public:
			bool isDone() const {
				// L'iteration se place � la bonne carte ?
				return i == Jeu::getInstance().getNbCartes(); 
			}

			void next() {
				if (isDone()) {
					throw SetException("Iteration termin�");
				}
				i++;
			}

			const Carte& currentItem() const {
				if (isDone()) {
					throw SetException("Iteration termin�");
				}
				return Jeu::getInstance().getCarte(i);
			}
		};

		Iterator getIterator() const {
			return Iterator();
		}

		class FormeIterator {
			size_t i = 0;
			Forme forme;
			FormeIterator(Forme f) : forme(f) {
				while (!isDone() && Jeu::getInstance().getCarte(i).getForme() != forme) {
					i++;
				}
			}
			// Cela permet � un objet Jeu d'acceder au constructeur priv� de iterateur
			friend class Jeu;
		public:
			bool isDone() const {
				// L'iteration se place � la bonne carte ?
				return i == Jeu::getInstance().getNbCartes();
			}

			void next() {
				if (isDone()) {
					throw SetException("Iteration terminé");
				}
				i++;
				while (!isDone() && Jeu::getInstance().getCarte(i).getForme() != forme) {
					i++;
				}
			}

			const Carte& currentItem() const {
				if (isDone()) {
					throw SetException("Iteration terminé");
				}
				return Jeu::getInstance().getCarte(i);
			}
		};

		FormeIterator getIterator(Forme f) const {
			return FormeIterator(f);
		}

		// por que static ? Ademas, no se puede definir en cpp
		// toujours publique
		static Jeu& getInstance();
		// Methode de liberation de l'instance
		static void libererInstance();

		size_t getNbCartes() const { return 81; }


	};

	class Pioche {
		const Carte** cartes = nullptr;
		size_t nb = 0;
	public:
		//explicit Pioche(const Jeu& j);
		Pioche();
		size_t getNbCartes() const { return nb; }
		bool estVide() const { return nb == 0; }
		const Carte& piocher();
		~Pioche();
	};

	class Plateau {
		const Carte** cartes = nullptr;
		size_t nbMax = 0;
		size_t nb = 0;
	public:
		class const_iterator {
			const Carte** current = nullptr;
			const_iterator(const Carte** c) : current(c) {}
			friend class Plateau;
		public:
			const_iterator& operator++() { current++; return *this; }
			const Carte& operator*() { return **current; }
			bool operator!=(const_iterator it) { return current != it.current; }
		};

		const_iterator begin() const { return const_iterator(cartes); }
		const_iterator end() const { return const_iterator(cartes + nb); }

		Plateau() = default;
		size_t getNbCartes() const { return nb; }
		void ajouter(const Carte& c);
		void retirer(const Carte& c);
		void print(ostream& f = cout) const;
		Plateau(const Plateau& p);
		Plateau& operator=(const Plateau& p);
		~Plateau() { delete[] cartes; };
	};

	class Combinaison {
		const Carte* c1;
		const Carte* c2;
		const Carte* c3;
	public:
		Combinaison(const Carte& _c1, const Carte& _c2, const Carte& _c3) :c1(&_c1), c2(&_c2), c3(&_c3) {}
		bool estUnSET() const;
		const Carte& getCarte1() const { return *c1; }
		const Carte& getCarte2() const { return *c2; }
		const Carte& getCarte3() const { return *c3; }
		~Combinaison() = default;
		Combinaison(const Combinaison& c) = default;
		Combinaison& operator=(const Combinaison& c) = default;
	};

	class Controleur {
		//Jeu jeu;
		Plateau plateau;
		Pioche* pioche;
	public:
		Controleur();
		void distribuer();
		~Controleur() { delete pioche; }
		const Plateau& getPlateau() const { return plateau; }
	};

	ostream& operator<<(ostream& f, const Set::Carte& c);


	ostream& operator<<(ostream& f, const Set::Combinaison& c);
	ostream& operator<<(ostream& f, const Set::Plateau& c);
}


#endif
