#ifndef _SET_H
#define _SET_H

#include <iostream>
#include <string>
#include <initializer_list>
#include <array>
#include <cstdlib>
using namespace std;

namespace Set {
	// classe pour g�rer les exceptions dans le set
	class SetException {
	public:
		SetException(const string& i) :info(i) {}
		string getInfo() const { return info; }
	private:
		string info;
	};

	// caract�ristiques
	enum class Couleur { rouge, mauve, vert };
	enum class Nombre { un=1, deux=2, trois=3 };
	enum class Forme { ovale, vague, losange };
	enum class Remplissage { plein, vide, hachure };

	// conversion d'une caract�ristique en string
	string toString(Couleur c);
	string toString(Nombre v);
	string toString(Forme f);
	string toString(Remplissage v);

	// �criture d'une caract�ristique sur un flux ostream
	ostream& operator<<(ostream& f, Couleur c);
	ostream& operator<<(ostream& f, Nombre v);
	ostream& operator<<(ostream& f, Forme x);
	ostream& operator<<(ostream& f, Remplissage r);

	// listes contenant les valeurs possibles pour chacune des caract�ristiques
	extern std::initializer_list<Couleur> Couleurs;
	extern std::initializer_list<Nombre> Nombres;
	extern std::initializer_list<Forme> Formes;
	extern std::initializer_list<Remplissage> Remplissages;

	// affichage des valeurs possibles pour chaque caract�ristiques
	void printCouleurs(std::ostream& f = cout);
	void printNombres(std::ostream& f = cout);
	void printFormes(std::ostream& f = cout);
	void printRemplissages(std::ostream& f = cout);

	class Carte {
	private:
		const Couleur couleur;
		const Forme forme;
		const Nombre nombre;
		const Remplissage remplissage;

	public:
		// Contructeur
		Carte(Nombre n, Couleur c, Forme f, Remplissage r) :
			nombre(n), couleur(c), forme(f), remplissage(r) {}

		// Getters
		Couleur getCouleur() const { return couleur; }
		Forme getForme() const { return forme; }
		Nombre getNombre() const { return nombre; }
		Remplissage getRemplissage() const { return remplissage; }

		// Constr. de recopie par defaut : types primitives (optionnel)
		Carte(const Carte& c) = default;

		// Operateur d'affectation (optionnel)
		Carte& operator=(const Carte& c) = default;

		// Destructeur par defaut (optionnel)
		~Carte() = default;
	};

	ostream& operator<<(ostream& f, const Carte& c);

	class Jeu {
	private:
		const Carte* cartes[81];

	public:
		// On va efectuer l'iteration des enumerations pour atteindre les 81 combinaisons
		// Definition en .cpp
		Jeu();

		// Getter
		size_t getNbCartes() const { return 81; }

		// Obtenir la reference ur la carte pont�e par le ieme pointeur
		const Carte& getCarte(size_t i) const;

		// "Il est impossible de dupliquer un objet Jeu par construction (recopie) ou affectation"
		Jeu(const Jeu& j) = delete;
		Jeu& operator=(const Jeu& j) = delete;

		// Destruction n�cessaire car on a fait de l'allocation dynamique
		~Jeu();
	};

	// Agregation avec la classe cartes. Elle ne g�re pas le cycle de vie
	// Elle va agreger certain numero de cartes
	class Pioche {
	private:
		const Carte** cartes = nullptr;
		size_t nb = 0;

	public:
		// explicit : naissance d'un objet
		// Uniuement avec les constructeurs � un parametre
		// type du parametre converti vers le type de la classe par le compilateur
		// explicit : on empeche la conversion implicite
		explicit Pioche(const Jeu& j);

		size_t getNbCartes() const { return nb; }
		bool estVide() const { return nb == 0; }
		const Carte& piocher();

		// Destructeur n�cessaire car on a allou� un tableau dynamique
		~Pioche();

		// Interdire la construction par recopie ou l'affectation
		Pioche(const Pioche& j) = delete;
		Pioche& operator=(const Pioche& j) = delete;

	};

	class Plateau {
	private:
		const Carte** cartes = nullptr;
		size_t nb = 0;
		size_t nbMax = 0;

	public:
		Plateau() = default;
		~Plateau();
		size_t getNbCartes() const { return nb; }
		void ajouter(const Carte& c);
		void retirer(const Carte& c);
		void print(ostream& f = cout) const;

		// Construction par recopie ou l�affectation sont permis
		Plateau(const Plateau& p);
		Plateau& operator=(const Plateau& p);
	};

	ostream& operator<<(ostream& f, const Plateau& p);
}


#endif