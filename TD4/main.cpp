#include "set.h"
#include <time.h>
using namespace Set;

int main() {
	try {
		srand(time(NULL));
		Carte c(Nombre::deux, Couleur::mauve, Forme::losange, Remplissage::hachure);
		cout << c;

		// Valide car on pointe vers des objets Cartes d�j� initialis�s
		Carte* tab2[10];
		// Carte** tabDyn = new Carte * [10]; 
	}
	catch (SetException& e) {
		std::cout << e.getInfo() << "\n";
	}
	
	
	return 0;
}

// 2. Constructeur defini avec 4 arguments, on n'a pas de constructeur par defaut, deux erreurs :
// 
//		Carte c2;
//		Carte tab[10];

// UML : Les differentes enumeration sont des composants de la carte

// 3. Un jeu est compos� de 81 cartes
// La classe Jeu compose la classe Carte (elle g�re le cycle de vie)

// 4. La valeur point�e va �tre const. Une fois les cartes initilis�es, on ne pourra pas les modifier