﻿#include "set.h"

namespace Set {
	
	std::initializer_list<Couleur> Couleurs = { Couleur::rouge, Couleur::mauve, Couleur::vert };
	std::initializer_list<Nombre> Nombres = { Nombre::un, Nombre::deux, Nombre::trois };
	std::initializer_list<Forme> Formes = { Forme::ovale, Forme::vague, Forme::losange };
	std::initializer_list<Remplissage> Remplissages = { Remplissage::plein, Remplissage::vide, Remplissage::hachure };

	string toString(Couleur c) {
		switch (c) { 
		case Couleur::rouge: return "R";
		case Couleur::mauve: return "M";
		case Couleur::vert: return "V";
		default: throw SetException("Couleur inconnue");
		}
	}

	string toString(Nombre v) {
		switch (v) {
		case Nombre::un: return "1";
		case Nombre::deux: return "2";
		case Nombre::trois: return "3";
		default: throw SetException("Nombre inconnue");
		}
	}

	string toString(Forme f) {
		switch (f) {
		case Forme::ovale: return "O";
		case Forme::vague: return "~";
		case Forme::losange: return "\004";
		default: throw SetException("Forme inconnue");
		}
	}

	string toString(Remplissage r) {
		switch (r) {
		case Remplissage::plein: return "P";
		case Remplissage::vide: return "_";
		case Remplissage::hachure: return "H";
		default: throw SetException("Remplissage inconnu");
		}
	}

	std::ostream& operator<<(std::ostream& f, Couleur c) { f << toString(c); return f; }
	std::ostream& operator<<(std::ostream& f, Nombre v) {	f << toString(v); return f; }
	std::ostream& operator<<(std::ostream& f, Forme x) { f << toString(x);  return f; }
	std::ostream& operator<<(std::ostream& f, Remplissage r) { f << toString(r); return f; }

	void printCouleurs(std::ostream& f) {
		for (auto c : Couleurs) f << c << " ";
		f << "\n";
	}

	void printNombres(std::ostream& f) {
		for (auto v : Nombres) f << v << " ";
		f << "\n";
	}

	void printFormes(std::ostream& f) {
		for (auto x : Formes) f << x << " ";
		f << "\n";
	}

	void printRemplissages(std::ostream& f) {
		for (auto r : Remplissages) f << r << " ";
		f << "\n";
	}

	ostream& operator<<(ostream& f, const Carte& c) {
		f << "(" << c.getNombre() << "," << c.getCouleur() << "," << c.getForme() << "," << c.getRemplissage() << ")";
		return f;
	}

	// On va efectuer l'iteration des enumerations pour atteindre les 81 combinaisions
	Jeu::Jeu() : cartes() {
		size_t i = 0;
		for (auto n : Nombres) {
			for (auto c : Couleurs) {
				for (auto f : Formes) {
					for (auto r : Remplissages) {
						// Postfix : va incrementeer sa valeur après l'allocation dynamique
						cartes[i++] = new Carte(n, c, f, r);
					}
				}
			}

		}

	}

	const Carte& Jeu::getCarte(size_t i) const {
		if (i <0 || i >= 81) throw SetException("Numero superieur à 81");
		
		// * on accede à lvalue avec le dereferencement
		return *cartes[i];
	}

	Jeu::~Jeu() {
		// Alternative 1 : privileger cette option
		for (size_t i = 0; i < getNbCartes(); i++)	delete cartes[i];
		
		// Alernative 2 :
		// delete[] cartes;
	}

	Pioche::Pioche(const Jeu& j) : 
		cartes(new const Carte* [j.getNbCartes()] + 1), nb(j.getNbCartes()) {
		//							+1 : Para agregar 8 octets au tableau
		for (size_t i = 0; i < nb; i++)
			cartes[i] = &j.getCarte(i);
	}

	const Carte& Pioche::piocher() {
		if (estVide()) throw SetException("La pioche est vide");
		size_t x = rand() % nb; // On tire une position entre 0 et nb - 1
		const Carte* c = cartes[x]; // On retient l'adresse de la carte dans une Carte* temporaire
		
		// On décle les pointeurs de cartes à parir de la position x
		for (size_t i = x + 1; i < nb; i++)
			cartes[i - 1] = cartes[i];

		nb--;
		return *c;
	}

	Pioche::~Pioche() { delete[] cartes; }

	void Plateau::ajouter(const Carte& c) {
		// Dans le cas où on a dépassé la taille maximale

		if (nb == nbMax) {
			// 1. Créer un tableau avec le double de capacité
			const Carte** newtab = new const Carte * [(nbMax + 1) * 2];

			// 2. Recopie de l'ancien tableau
			for (size_t i = 0; i < nb; i++)
				newtab[i] = cartes[i];

			// 3. On suprime l'ancien tableau et on augmente le nbMax
			auto old = cartes;
			cartes = newtab;
			nbMax = (nbMax + 1) * 2;
			delete[] old;
		}

		// On a un pointeur de pointeurs, donc on a besoin de l'adresse de la reference
		cartes[nb++] = &c;
	}

	// On va chercher la carte à retirer dans notre plateau
	void Plateau::retirer(const Carte& c) {
		size_t i = 0;

		// 1ere condition : on n'a pas trouvé la carte et il ne reste plus de cartes
		// 2eme condition : on compare chaque carte du tableau avec la carte envoyé par référence
		while (i < nb && cartes[i] != &c) i++;
		if (i == nb) throw SetException("Carte inexistante");
		
		// On retire la carte, il n'y a pas besoin de se souvenir de l'adresse de la carte
		while (i < nb) {
			cartes[i - 1] = cartes[i];
			i++;
		}
		nb--;
	}

	void Plateau::print(ostream& f) const {
		for (size_t i = 0; i < nb; i++) {
			if (i % 4 == 0) {
				f << endl;
			}
			f << *cartes[i] << " ";
		}
		f << endl;
	}

	ostream& operator<<(ostream& f, const Plateau& p) {
		p.print(f);
		return f;
	}

	Plateau::Plateau(const Plateau& p) : 
		cartes(new const Carte*[p.nb]), nb(p.nb), nbMax(p.nbMax) {
		for (size_t i = 0; i < nb; i++) {
			cartes[i] = p.cartes[i];
		}
	}

	Plateau& Plateau::operator=(const Plateau& p) {
		// Instruction de sécurité pour l'autoaffectation
		if (this != &p) {

			// Notre tableau destination est-il suffissament grande ?
			if (p.nb > nbMax) {
				// non, on n'a pas de space
				const Carte** newtab = new const Carte * [p.nb];
				for (size_t i = 0; i < p.nb; i++) {
					newtab[i] = p.cartes[i];
				}
				auto old = cartes;
				cartes = newtab;
				nb = nbMax = p.nb;
				delete[] old;
			}
			else {
				// Oui, on a assez d'espace
				for (size_t i = 0; i < p.nb; i++) {
					cartes[i] = p.cartes[i];
				}
				nb = p.nb;
			}
		}
		return *this;
	}

	Plateau::~Plateau() { delete[] cartes; }


}