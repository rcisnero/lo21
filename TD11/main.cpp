#include <iostream>
#include "evenement.h"

using namespace std;
using namespace TIME;

int main() {
	/*Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	Evt1jDur e3(Date(11, 6, 2013), "Lancement de Longue Marche", Horaire(17, 38), Duree
	(0, 10));
	e1.afficher();
	e2.afficher();
	e3.afficher();*/

	/*Rdv e(Date(11, 11, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV",
		"bureau");
		std::cout << "RDV:";
	e.afficher();*/

	/*Evt1j* tab[2];
	Evt1j evt(Date(1 , 12 , 2021), "TD");
	Rdv rdv(Date(2, 12, 2021), "examen", Horaire(8, 0), Duree(90), "Intervenants UV",
		"HDS");
	tab[0] = &evt;
	tab[1] = &rdv;
	for (size_t i = 0; i < sizeof(tab) / sizeof(tab[0]); i++)
		tab[i]->afficher();*/

	/*Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	Evt1jDur e3(Date(11, 6, 2013), "Lancement de Longue Marche", Horaire(17, 38), Duree
	(0, 10));
	Rdv e4(Date(11, 4, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");
	//e1.afficher(); e2.afficher(); e3.afficher(); e4.afficher();
	Evt1j * pt1 = &e1; Evt1j * pt2 = &e2; Evt1j * pt3 = &e3; Evt1j * pt4 = &e4;
	//pt1->afficher(); pt2->afficher(); pt3->afficher(); pt4->afficher();
	cout << e1 << e2 << e3 << e4;*/

	/*Rdv* pt5 = new Rdv(Date(12, 11, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV","bureau");
	pt5->afficher();
	delete pt5;

	Evt1j * pt6 = new Rdv(Date(12, 11, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV","bureau");
	pt6->afficher();
	delete pt6;*/

	/*Agenda a;
	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Rdv e2(Date(11, 4, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");
	EvtPj e3(Date(1, 10, 1957), Date(10, 10, 1957), "Oktoberfest");
	a << e1 << e2 << e3;
	a.afficher();*/

	/*
	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1jDur e2(Date(4, 4, 2012), "courses", Horaire(19, 0), Duree(45));
	Rdv e3(Date(11, 4, 2012), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");
	Evt1j e4(Date(20, 7, 1969), "Premier Homme sur la Lune");
	Evt1j e5(Date(14, 7, 1789), "Prise de la Bastille");
	Rdv e6(Date(9, 4, 2012), "reunion", Horaire(10, 45), Duree(1, 30), "etudiant", "passerelle"
	);
	Evt1jDur e7(Date(6, 12, 2012), "St Nicolas", Horaire(19, 0), Duree(45));
	EvtPj e8(Date(21, 6, 2010), Date(22, 6, 2010), "examens");

	Rdv e9(Date(19, 5, 2013), "rdv", Horaire(11, 45), Duree(2, 30), "Mr X", "Utseus");
	Agenda mon_agenda;
	mon_agenda << e1 << e2 << e3 << e4 << e5 << e6 << e7 << e8 << e9;
	//iterator
	for (auto it = mon_agenda.begin(); it != mon_agenda.end(); ++it) {  */
		/*if (it != mon_agenda.begin()) // Test de l'itérateur dans l'autre sens. Ca marche : boucle infinie.
			it--;*/
	/*
		cout << *it << "\n";
	}
	// const_iterator
	for (auto it = mon_agenda.cbegin(); it != mon_agenda.cend(); ++it) cout << *it << "\n";
	// maintenant, on peut aussi utiliser un range for
	for (auto& e : mon_agenda) cout << e << "\n";
	*/

	// Exercice 37
	// 
	cout << "Exercice 37" << endl;
	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	Evt1jDur e3(Date(11, 6, 2013), "Lancement de Longue Marche", Horaire(17, 38), Duree(0, 10));
	Rdv e4(Date(11, 4, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");

	Evt1j* pt1 = &e1; Evt1j * pt2 = &e2; Evt1j * pt3 = &e3; Evt1j * pt4 = &e4;
	Evt1j& ref1 = e1; Evt1j & ref2 = e2; Evt1j & ref3 = e3; Evt1j & ref4 = e4;

	Evt1jDur* pt = dynamic_cast<Evt1jDur*>(pt1); if (pt) pt->afficher();
	pt = dynamic_cast<Evt1jDur*>(pt2); if (pt) pt->afficher();
	pt = dynamic_cast<Evt1jDur*>(pt3); if (pt) pt->afficher();
	pt = dynamic_cast<Evt1jDur*>(pt4); if (pt) pt->afficher();

	try { Rdv& r1 = dynamic_cast<Rdv&>(ref1); r1.afficher(); }
	catch (bad_cast & e) { cout << e.what() << endl; }
	try { Rdv& r2 = dynamic_cast<Rdv&>(ref2); r2.afficher(); }
	catch (bad_cast & e) { cout << e.what() << endl; }
	try { Rdv& r3 = dynamic_cast<Rdv&>(ref3); r3.afficher(); }
	catch (bad_cast & e) { cout << e.what() << endl; }
	try { Rdv& r4 = dynamic_cast<Rdv&>(ref4); r4.afficher(); }
	catch (bad_cast & e) { cout << e.what() << endl; }

	// Exercice 39
	cout << "Exercice 39" << endl;
	

	return 0;
}