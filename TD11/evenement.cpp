#include "evenement.h"

std::ostream& operator<<(std::ostream& f, const TIME::Evt& e) {
	e.afficher(f);
	return f;
}

TIME::Agenda& TIME::Agenda::operator<<(const TIME::Evt& e) {
	// Avant : tab.push_back(&e); -> ce n'est pas possible de stocker un evenement const
	// Avec e.clone(), on peut envoyer des const en parametre
	tab.push_back(e.clone());
	nbEvts++;
	return *this;
}

void TIME::Agenda::afficher(std::ostream& f) const {
	f << "### AGENDA ###" << std::endl;
	for (unsigned int i = 0; i < nbEvts; i++) {
		tab[i]->afficher(f);
	}
	f << "### FIN AGENDA ###" << std::endl;
}

TIME::Evt1j* TIME::Evt1j::clone() const { return new Evt1j(*this); }
TIME::Evt1jDur* TIME::Evt1jDur::clone() const { return new Evt1jDur(*this); }
TIME::EvtPj* TIME::EvtPj::clone() const { return new EvtPj(*this); }
TIME::Rdv* TIME::Rdv::clone() const { return new Rdv(*this); }

TIME::Agenda::~Agenda() {
	for (size_t i = 0; i < nbEvts; i++) {
		// On desalloue chaque pointeur au lieu d'utiliser delete[]
		delete tab[i];
	}
}

std::string TIME::Evt1j::toString() const {
	std::stringstream f;
	f << "***** Evt1j ********" << std::endl;
	f << "Date = " << getDate() << ", sujet = " << getDescription() << std::endl;
	return f.str();
}

std::string TIME::EvtPj::toString() const {
	std::stringstream f;
	f << "***** EvtPj ********" << std::endl;
	f << "Date debut = " << getDateDebut() << "Date fin = " << getDateFin() << ", sujet = " << getDescription() << std::endl;
	return f.str();
}

std::string TIME::Evt1jDur::toString() const {
	std::stringstream f;
	f << Evt1j::toString() << ", debut = " << getHoraire() << ", duree = " << getDuree() << std::endl;
	return f.str();
}

std::string TIME::Rdv::toString() const {
	std::stringstream f;
	f << Evt1jDur::toString() << ", personne = " << getPersonne() << ", lieu = " << getLieu() << std::endl;
	return f.str();
}

