#if !defined(MYLOG_H)
#define MYLOG_H
#include "log.h"
#include "evenement.h"

// connexion entre MyLog et Agenda : <<est une implementation de>>
                          // adaptateur de classe
class MyLog : public Log/*, private TIME::Agenda */ {
private:
	TIME::Agenda evts; // adaptateur d'objet 
public:
	void addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s);
	void displayLog(std::ostream& f) const;
};

#endif