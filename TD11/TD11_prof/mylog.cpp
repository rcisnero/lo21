#include "mylog.h"

void MyLog::addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s) {
	if (evts.begin() != evts.end()) {
		const TIME::Evt1jDur& lastEvent = dynamic_cast<const TIME::Evt1jDur&>(**(--evts.end()));
		if (d < lastEvent.getDate() || (d == lastEvent.getDate() && h < lastEvent.getHoraire())) {
			throw LogError("Attempt of addition of an anterior event : denied\n");
		}
	}
	evts << TIME::Evt1jDur(d, s, h, TIME::Duree(0));
}
void MyLog::displayLog(std::ostream& f) const {
	for (TIME::Agenda::const_iterator it = evts.cbegin(); it != evts.cend(); ++it)
		f << dynamic_cast<const TIME::Evt1jDur&>(**it).getDate()
		<< " - " << dynamic_cast<const TIME::Evt1jDur&>(**it).getHoraire()
		<< " : " << (**it).getDescription() << std::endl << std::endl;
	if (evts.cbegin() == evts.cend())
		std::cout << "No event recorded" << std::endl << std::endl;
}