#if !defined(MYLOG_H)
#define MYLOG_H
#include "log.h"
#include "evenement.h"

class MyLog : public Log/*, private TIME::Agenda*/ {
private:
	TIME::Agenda evts;
public:
	void addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s);
	void displayLog(std::ostream& f=std::cout) const;
//	unsigned int getNbEvts() const {evts.begin()};
};

#endif