#if !defined(_EVENEMENT_H)
#define _EVENEMENT_H

#include <iostream>
#include <string>
#include <vector>
#include "timing.h"
#include <sstream>

namespace TIME {

	class Evt {
		std::string sujet;
	public:
		Evt(const std::string& s):sujet(s){}
		//virtual void afficher(std::ostream& f = std::cout) const = 0;
		const std::string& getDescription() const { return sujet; }
		virtual ~Evt() {};
		virtual Evt* clone() const = 0;
		virtual std::string toString() const = 0;

		// Maintenant, on appelle la methode virtuelle pure toString()
		void afficher(std::ostream& f = std::cout) const {
			f << toString();
		}
	};

	class Evt1j : public Evt {
	private:
		Date date;
	public:
		Evt1j(const Date& d, const std::string& s) :Evt(s), date(d) {
			std::cout << "construction d'un objet Evt1j" << std::endl;
		}
		~Evt1j(){ std::cout << "destruction d'un objet Evt1j" << std::endl; }
		const Date& getDate() const { return date; }
		// Maintenant, on appelle la methode virtuelle pure toString()
		/*void afficher(std::ostream& f = std::cout) const {
			//f << "***** Evt ********" << "\n" << "Date=" << date << " sujet=" << getDescription() << "\n";
			f << toString();
		}*/
		Evt1j* clone() const;
		std::string toString() const;

	};

	class EvtPj : public Evt {
		Date debut;
		Date fin;
	public:
		EvtPj(const Date& d, const Date& f, const std::string& s) :Evt(s), debut(d), fin(f) {
			std::cout << "construction d'un objet EvtPj" << std::endl;
		}
		~EvtPj() { std::cout << "destruction d'un objet EvtPj" << std::endl; }
		const Date& getDateDebut() const { return debut; }
		const Date& getDateFin() const { return fin; }
		// Maintenant, on appelle la methode virtuelle pure toString()
		/*virtual void afficher(std::ostream& f = std::cout) const {
			f << "***** Evt ********" << "\n" << "Date debut=" << debut << " Date fin=" << fin << " sujet=" << getDescription() << "\n";
		}*/
		EvtPj* clone() const;
		std::string toString() const;
	};

	class Evt1jDur : public Evt1j {
	private :
		Horaire debut;
		Duree duree;
	public:
		Evt1jDur(const Date& d, const std::string& s, const Horaire& h, const Duree& t):
			Evt1j(d,s),//appel au constructeur de la classe de base
			debut(h),duree(t) {
			std::cout << "construction d'un objet Evt1jDur" << std::endl;
		}
		~Evt1jDur() { std::cout << "destruction d'un objet Evt1jDur" << std::endl; }
		const Horaire& getHoraire() const { return debut; }
		const Duree& getDuree() const { return duree; }
		// Maintenant, on appelle la methode virtuelle pure toString()
		/*void afficher(std::ostream& f = std::cout) const {
			//f << "***** Evt ********" << "\n" << "Date=" << getDate() << " sujet=" << getDescription() << "\n";
			Evt1j::afficher(f); // rappel de la m�thode de la classe de base
			f << "debut=" << debut << " duree=" << duree << std::endl;
		}*/
		Evt1jDur* clone() const;
		std::string toString() const;
	};

	class Rdv : public Evt1jDur {
	private:
		std::string	personne;
		std::string lieu;
	public:
		Rdv(const Date& d, const std::string& s, const Horaire& h, const Duree& t,
			const std::string& p, const std::string& l) :
			Evt1jDur(d, s, h, t), personne(p), lieu(l) {
			std::cout << "construction d'un objet Rdv" << std::endl;
		}
		~Rdv() { std::cout << "destruction d'un objet Rdv" << std::endl; }
		const std::string& getPersonne() const { return personne; }
		const std::string& getLieu() const { return lieu; }
		// Maintenant, on appelle la methode virtuelle pure toString()
		/*void afficher(std::ostream& f = std::cout) const {
			Evt1jDur::afficher(f); // rappel de la m�thode de la classe de base
			f << "personne=" << personne << " lieu=" << lieu << std::endl;
		}*/
		Rdv* clone() const;
		std::string toString() const;
	};

	class Agenda {
		std::vector<Evt*> tab;
		unsigned int nbEvts = 0;
	public:
		Agenda() = default; //Agenda() : tab() {}
		~Agenda();
		Agenda(const Agenda&) = delete;
		Agenda& operator=(const Agenda&) = delete;
		Agenda& operator<<(const Evt& e);
		void afficher(std::ostream& f = std::cout) const;

		class iterator : public std::vector<Evt*>::iterator {
		public:
			Evt& operator*() const { // Permet � l'op�rateur d'indirection de renvoyer une &Evt au lieu d'un *Evt (le vector tab contient en effet des *Evt)
				return *std::vector<Evt*>::iterator::operator*();
			}
		private:
			friend class Agenda;
			iterator(const std::vector<Evt*>::iterator& it):std::vector<Evt*>::iterator(it){}
		};
		iterator begin() { return iterator(tab.begin()); }
		iterator end() { return iterator(tab.end()); }

		class const_iterator : public std::vector<Evt*>::const_iterator {
		public:
			/*Evt& operator*() const {
				return *std::vector<Evt*>::const_iterator::operator*();
			}*/
		private:
			friend class Agenda;
			const_iterator(const std::vector<Evt*>::const_iterator& it) :std::vector<Evt*>::const_iterator(it) {}
		};
		const_iterator cbegin() const { return const_iterator(tab.begin()); }
		const_iterator cend() const { return const_iterator(tab.end()); }
	};

	inline Date getDate(const Evt& e) {
		// On recupere la date des differentes classes � travers d'un down-casting
		const Evt1j* pt1 = dynamic_cast<const Evt1j*>(&e);
		const EvtPj* pt2 = dynamic_cast<const EvtPj*>(&e);
		if (pt1) return pt1->getDate();
		if (pt2) return pt2->getDateDebut();
		throw "Type evt inattendu";
	}

	inline bool operator<(const Evt& e1, const Evt& e2) {
		Date d1 = getDate(e1);
		Date d2 = getDate(e2);
		if (d1 < d2) return true;
		if (d2 < d1) return false;
		// d1 == d2
		const Evt1jDur* pt1 = dynamic_cast<const Evt1jDur*>(&e1);
		const Evt1jDur* pt2 = dynamic_cast<const Evt1jDur*>(&e2);

		if (pt1 == nullptr && pt2 == nullptr) return true;
		if (pt1 != nullptr && pt2 == nullptr) return false;
		if (pt1 == nullptr && pt2 != nullptr) return true;
		// Ce sont deux evt avec un horaire, on les compare
		return pt1->getHoraire() < pt2->getHoraire();
	}
}

std::ostream& operator<<(std::ostream& f,const TIME::Evt& e);
#endif
