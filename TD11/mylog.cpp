#include "mylog.h"

void MyLog::addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s) {
	// On utilise la methode operator<< appartenant à la classe Agenda (interface à adapter)
    // Ici, evts est notre objetAdapté
    evts << TIME::Evt1jDur(d, s, h, TIME::Duree(0));
}

void MyLog::displayLog(std::ostream& f) const {
	for (TIME::Agenda::const_iterator it = evts.cbegin(); it != evts.cend(); ++it)
		//Avec **it, on recupere l'evenement sur lequel it itere
		f << dynamic_cast<const	TIME::Evt1jDur&>(**it).getDate()
		<< " - " << dynamic_cast<const	TIME::Evt1jDur&>(**it).getHoraire()
		<< " : " << (**it).getDescription() << std::endl;
	

}