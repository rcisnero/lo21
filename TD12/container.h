#if !defined(_Container_T_H)
#define _Container_T_H
#include<string>
#include<stdexcept>

namespace TD {
	class ContainerException : public std::exception {
	protected:
		std::string info;
	public:
		ContainerException(const std::string& i = "") noexcept :info(i) {}
		const char* what() const noexcept { return info.c_str(); }
		~ContainerException()noexcept {}
	};

	template <class T>
	class Container {
	protected:
		// Protected : element priv� pour l'utilsateur mais public pour les classes deriv�es
		size_t nbEl;
	
	public:
		size_t size() const { return nbEl; }
		bool empty() const { return nbEl == 0; }
		Container(size_t n = 0) : nbEl(n) {}
		virtual ~Container() {}
		
		// Methodes virtuelles pures
		virtual T& element(size_t i) = 0; 
		virtual const T& element(size_t i) const = 0;
		virtual void push_back(const T& x) = 0;
		virtual void pop_back() = 0;

		virtual T& front();
		virtual const T& front() const; 
		virtual T& back();
		virtual const T& back() const; 
		virtual void clear() { while (!empty()) pop_back(); }
	};

	template <class T>
	class Vector : public Container<T> {
	private:
		T* tab;
		size_t cap;

	public:
		Vector(size_t n = 0, const T& x = T());
		Vector(const Vector<T>& v);
		~Vector();
		Vector<T>& operator=(const Vector<T>& v);
		T& operator[](size_t i);
		const T& operator[](size_t i) const;
		T& element(size_t i) override;
		const T& element(size_t i) const override;
		void push_back(const T& x) override;
		void pop_back() override;

		using iterator = T*;
		using const_iterator = const T*;
		iterator begin() { return tab; }
		iterator end() { return tab + this->nbEl; }
		const_iterator begin() const { return tab; }
		const_iterator end() const { return tab + this->nbEl; }
	};

	namespace AO {
		template<class T, class CONT = Vector<T>>
		class Stack : private Vector<T> {
			CONT cont;
			bool empty() const { return cont.empty(); }
			void push(const T& x) { return cont.push_back(); }
			void pop() { return cont.pop_back(); }
			size_t size() const { return cont.size(); }
			T& top() { return cont.back(); }
			const T& top() const { return cont.back(); }
			void clear() { return cont.clear(); }

		};
	}

    /*
	namespace AC {
		template<class T, class CONT = Vector<T>>
		class Stack : private CONT {
			bool empty() const { return CONT<T>::empty(); }
			void push(const T& x) { return this->push_back(); }
			void pop() { return this->pop_back(); }
			size_t size() const { return CONT<T>::size(); }
			T& top() { return this->back(); }
			const T& top() const { return this->back(); }
			void clear() { return CONT<T>::clear(); }
			
		};
	}
    */

	template<class IT>
	IT element_minimum(IT it1, IT it2) {
		IT itmin = it1;
		while (it1 != it2) {
			if (*it1 < *itmin) itmin = it1;
			++it1;
		}
		return itmin;
	}

	// FUNC est le type de la fonction de comparaison
	template<class IT, class FUNC>
	IT element_minimum(IT it1, IT it2, FUNC comp) {
		IT itmin = it1;
		// comp est la fonction de comparaison ou l'objet fonction ou une lambda
		while (it1 != it2) {
			if (comp(*it1, *itmin)) itmin = it1;
			++it1;
		}
		return itmin;
	}

	// Override : redefinition d'une methode virtuelle
	/** 
	* Constructeur de vecteur :
	* Si l'utilisateur indique une taille pour le vecteur mais pas de valeurs,
	* on va appeler le constructeur par d�faut de type T
	*/
}

/**
* Definition inline des methodes de la classe patron
* - Avantages : on evite de recopier les methodes pour chaque type du container
* - Desavantages : le fichier .h est copi� sur chaque unit� de compilation
*/
template<class T> T& TD::Container<T>::front() {
	if (!empty()) return element(0);
	throw ContainerException("Erreur : demande hors limite (container vide)");
}

template<class T> const T& TD::Container<T>::front() const {
	if (!empty()) return element(0);
	throw ContainerException("Erreur : demande hors limite (container vide)");
}

template<class T> T& TD::Container<T>::back() {
	if (!empty()) return element(nbEl - 1);
	throw ContainerException("Erreur : demande hors limite (container vide)");
}

template<class T> const T& TD::Container<T>::back() const {
	if (!empty()) return element(nbEl - 1);
	throw ContainerException("Erreur : demande hors limite (container vide)");
}

template<class T> TD::Vector<T>::Vector(size_t n, const T& x) : 
	Container<T>(n), tab(new T[n]), cap(n) {
	for (size_t i = 0; i < Container<T>::nbEl; i++) {
		tab[i] = x;
	}
}

template<class T> TD::Vector<T>::Vector(const TD::Vector<T>& v) : 
	Container<T>(v.nbEl), tab(new T[v.size()]), cap(v.size()) {
	for (size_t i = 0; i < Container<T>::size(); i++) {
		tab[i] = v.tab[i];
	}
}

template<class T> TD::Vector<T>::~Vector() {
	delete[] tab;
}

template<class T> TD::Vector<T>& TD::Vector<T>::operator=(const TD::Vector<T>& v) {
	if (this != &v) {
		T* newtab = new T[v.nbEl];
		for (size_t i = 0; i < v.nbEl; i++) {
			newtab[i] = v.tab[i];
		}
		cap = v.nbEl;
		Container<T>::nbEl = v.nbEl;
		T* old = tab;
		tab = newtab;
		delete[] old;
	}
	return *this;
}

template<class T> T& TD::Vector<T>::element(size_t i) {
	if (i < Container<T>::nbEl)	return tab[i];
	throw ContainerException("Erreur : le conteneur contient moins de i elements");
}

template<class T> const T& TD::Vector<T>::element(size_t i) const {
	if (i < Container<T>::nbEl)	return tab[i];
	throw ContainerException("Erreur : le conteneur contient moins de i elements");
}

template<class T> void TD::Vector<T>::push_back(const T& x) {
	if (Container<T>::nbEl == cap) {
		T* newtab = new T[cap + 5];
		for (size_t i = 0; i < Container<T>::nbEl; i++) {
			newtab[i] = tab[i];
		}
		cap += 5;
		T* old = tab;
		tab = newtab;
		delete[] old;
	}
	tab[Container<T>::nbEl++] = x;
}

template<class T> void TD::Vector<T>::pop_back() {
	if (!this->empty()) Container<T>::nbEl--;
	else throw ContainerException("Erreur : demande hors limite (container vide)");
}

template<class T> T& TD::Vector<T>::operator[](size_t i) {

}

template<class T> const T& TD::Vector<T>::operator[](size_t i) const {

}


#endif
