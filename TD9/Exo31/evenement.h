#pragma once
#if !defined(_EVENEMENT_H)
#define _EVENEMENT_H

#include <iostream>
#include <string>
#include "timing.h"

namespace TIME {
	class Evt1j {
	private:
		Date date;
		std::string sujet;
	public:
		Evt1j(const Date& d, const std::string& s) :date(d), sujet(s) {
			std::cout << "Construction d'un objet de la classe Evt1j\n";
		}
		~Evt1j() { std::cout << "Destruction d'un objet de la classe Evt1j\n";  }
		const std::string& getDescription() const { return sujet; }
		const Date& getDate() const { return date; }
		void afficher(std::ostream& f = std::cout) const {
			f << "***** Evt ********" << "\n" << "Date=" << date << " sujet=" << sujet << "\n";
		}
	};

	class Evt1jDur : public Evt1j{
	private:
		Horaire debut;
		Duree duree;
	public:
		Evt1jDur(const Date& d, const std::string& s, const Horaire& h, const Duree& dur):
			Evt1j(d,s), //appel au constructeur de la classe mere
			debut(h), duree(dur) {
			std::cout << "Construction d'un objet de la classe Evt1jDur\n";
		}
		~Evt1jDur() { std::cout << "Destruction d'un objet de la classe Evt1jDur\n"; }
		const Horaire& getHoraire() const { return debut; }
		const Duree& getDuree() const { return duree; }
		void afficher(std::ostream& f = std::cout) const {
			Evt1j::afficher(f); //rappel de la m�thode de la calsse de base
			f << "debut = " << debut << " duree = " << duree << "\n";
		}

	};

	class Rdv : public Evt1jDur {
	private:
		std::string personne;
		std::string lieu;
	public:
		Rdv(const Date& d, const std::string& s, const Horaire& h, const Duree& dur, 
			const std::string& p, const std::string& l) :
			Evt1jDur(d, s, h, dur),
			personne(p), lieu(l) {
			std::cout << "Construction d'un objet de la classe Rdv\n";
		}
		~Rdv() { std::cout << "Destruction d'un objet de la classe Rdv\n"; }
		const std::string& getPersonne() const { return personne; }
		const std::string& getLieu() const { return lieu; }
		void afficher(std::ostream& f = std::cout) const {
			Evt1jDur::afficher(f); //rappel de la m�thode de la calsse de base
			f << "personne = " << personne << " lieu = " << lieu << "\n";
		}
	};
}

#endif