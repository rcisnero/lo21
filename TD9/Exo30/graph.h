#pragma once
#if !defined(_GRAPH_H)
#define _GRAPH_H

#include<string>
#include<stdexcept>
#include<list>
#include<vector>
#include<iostream>
#include<algorithm>

using namespace std;

class GraphException : public exception {
	string info;

public:
	GraphException(const string& i) noexcept :info(i) {}
	virtual ~GraphException() noexcept {}
	const char* what() const noexcept { return info.c_str(); }
};

class Graph {
	// Vector : insertion plus difficile
	// list : equivalent � une liste double chain�e, insertion plus facile
	vector<list<unsigned int> > adj;
	unsigned int nb_edges;
	string name;
public:
	Graph(const string& n, size_t nb) : name(n), adj(nb), nb_edges(0) {}
	const string& getName() const { return name; }
	size_t getNbVertices() const { return adj.size(); }
	size_t getNbEdges() const { return nb_edges; }
	void addEdge(unsigned int i, unsigned int j);
	void removeEdge(unsigned int i, unsigned int j);
	const list<unsigned int>& getSuccessors(unsigned int i) const;
	const list<unsigned int> getPredecessors(unsigned int j) const;
};
ostream& operator<<(ostream& f, const Graph& G);

#endif