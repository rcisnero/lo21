#include "graph.h"

void Graph::addEdge(unsigned int i, unsigned int j){
	if (i >= getNbVertices() || j >= getNbVertices()) {
		throw GraphException("bad argument, a vertex of the edge is not defined");
	}
	// La fonction find() renvoie un pointeur vers la position de la valeur qu'on cherche
	// Si la valeur de retour est �gale � la end(), �a veut dire qu'elle n'est pas dans la liste.
	if (find(adj[i].begin(), adj[i].end(), j) != adj[i].end()) {
		throw GraphException("edge has already been defined");
	}

	adj[i].push_back(j);
	// Solution si on veut changer de conteneur
	//adj[i].insert(adj[i].end(), j);

	nb_edges++;
}

void Graph::removeEdge(unsigned int i, unsigned int j) {
	if (i >= getNbVertices() || j >= getNbVertices()) {
		throw GraphException("bad argument, a vertex of the edge is not defined");
	}
	list<unsigned int>::iterator it = find(adj[i].begin(), adj[i].end(), j);
	if (it != adj[i].end()) {
		adj[i].erase(it);
		nb_edges--;
	}
	else {
		throw GraphException("this edge has not been defined");
	}

}

const list<unsigned int>& Graph::getSuccessors(unsigned int i) const {
	if (i >= getNbVertices()) {
		throw GraphException("vertex is not defined");
	}
	// Le graphe est d�fini comme une liste de successeurs
	return adj[i];
}

const list<unsigned int> Graph::getPredecessors(unsigned int j) const {
	if (j >= getNbVertices()) {
		throw GraphException("vertex is not defined");
	}
	list<unsigned int> res;
	for (size_t i = 0; i < adj.size(); i++) {
		if (find(adj[i].begin(), adj[i].end(), j) != adj[i].end()) {
			res.insert(res.end(), i);
			// OU res.push_back(i);
		}
	}
	return res;
}

ostream& operator<<(ostream& f, const Graph& G) {
	f << "Graph " << G.getName() << " (" << G.getNbVertices() << " vertices and "
		<< G.getNbEdges() << " edges)" << endl;
	for (size_t i = 0; i < G.getNbVertices(); i++) {
		f << i << ": ";
		for (list<unsigned int>::const_iterator it = G.getSuccessors(i).begin(); 
			it != G.getSuccessors(i).end(); ++it) {
			f << *it << " ";
		}
		f << endl;
	}
	return f;
}
